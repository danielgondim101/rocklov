class Alert
  include Capybara::DSL

  def dark
    return find(".alert").text
  end
end
