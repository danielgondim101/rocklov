describe "POST/ signup" do
  context "novo usuario" do
    before(:all) do
      payload = { name: "Nina Hagen", email: "nina@gmail.com", password: "12345" }
      MongoDB.new.remove_user(payload[:email])
      @result = Signup.new.create(payload)
    end

    it "valida status code" do
      expect(@result.code).to eql 200
    end

    it "valida id do usuario" do
      expect(@result.parsed_response["_id"].length).to eql 24
    end
  end

  context "usuario ja existe" do
    before(:all) do
      #dado que eu tenho um novo usuário
      payload = { name: "João da Silva", email: "joao@ig.com.br", password: "12345" }
      MongoDB.new.remove_user(payload[:email])

      #e o email desse usuario ja foi cadastrado no sistema
      Signup.new.create(payload)

      #quando faço uma requisição para a rota
      @result = Signup.new.create(payload)
    end

    it "deve retornar 409" do
      #entao deve retornar 409
      expect(@result.code).to eql 409
    end

    it "deve retornar mensagem" do
      expect(@result.parsed_response["error"]).to eql "Email already exists :("
    end
  end

  examples = [
    {
      title: "nome não informado",
      payload: { name: "", email: "jose@gmail.com.br", password: "1234" },
      code: 412,
      error: "required name",
    },
    {
      title: "sem o campo nome",
      payload: { email: "jose@gmail.com.br", password: "1234" },
      code: 412,
      error: "required name",
    },
    {
      title: "senha nao informada",
      payload: { name: "José Manoel", email: "jose@gmail.com.br", password: "" },
      code: 412,
      error: "required password",
    },
    {
      title: "sem o campo senha",
      payload: { name: "José Manoel", email: "jose@gmail.com.br" },
      code: 412,
      error: "required password",
    },
    {
      title: "email nao informado",
      payload: { name: "José Manoel", email: "", password: "123 4" },
      code: 412,
      error: "required email",
    },
    {
      title: "sem o campo email",
      payload: { name: "José Manoel", password: "1234" },
      code: 412,
      error: "required email",
    },
  ]

  examples.each do |e|
    context "#{e[:title]}" do
      before(:all) do
        @result = Signup.new.create(e[:payload])
      end

      it "deve mostrar código #{e[:code]}" do
        expect(@result.code).to eql e[:code]
      end

      it "deve mostrar mensagem" do
        expect(@result.parsed_response["error"]).to eql e[:error]
      end
    end
  end
end

#context "nome não informado" do
#before(:all) do
#payload = { name: "", email: "jose@gmail.com.br", password: "1234" }
#MongoDB.new.remove_user(payload[:email])
#@result = Signup.new.create(payload)
#end

#   it "deve retornar mensagem" do
#     expect(@result.parsed_response["error"]).to eql "required name"
#   end
# end

# context "sem o campo nome" do
#   before(:all) do
#     payload = { email: "jose@gmail.com.br", password: "1234" }
#     MongoDB.new.remove_user(payload[:email])
#     #Signup.new.create(payload)
#     @result = Signup.new.create(payload)
#   end

#   it "deve retornar 412" do
#     expect(@result.code).to eql 412
#   end

#   it "deve retornar mensagem" do
#     expect(@result.parsed_response["error"]).to eql "required name"
#   end
# end

# context "senha não informada" do
#   before(:all) do
#     payload = { name: "José Manoel", email: "jose@gmail.com.br", password: "" }
#     MongoDB.new.remove_user(payload[:email])
#     #Signup.new.create(payload)
#     @result = Signup.new.create(payload)
#   end

#   it "deve retornar 412" do
#     expect(@result.code).to eql 412
#   end

#   it "deve retornar mensagem" do
#     expect(@result.parsed_response["error"]).to eql "required password"
#   end
# end

# context "sem o campo senha" do
#   before(:all) do
#     payload = { name: "José Manoel", email: "jose@gmail.com.br" }
#     MongoDB.new.remove_user(payload[:email])
#     @result = Signup.new.create(payload)
#   end

#   it "deve retornar 412" do
#     expect(@result.code).to eql 412
#   end

#   it "deve retornar mensagem" do
#     expect(@result.parsed_response["error"]).to eql "required password"
#   end

#   context "email não informado" do
#     before(:all) do
#       payload = { name: "José Manoel", email: "", password: "1234" }
#       MongoDB.new.remove_user(payload[:email])
#       @result = Signup.new.create(payload)
#     end

#     it "deve retornar 412" do
#       expect(@result.code).to eql 412
#     end

#     it "deve retornar mensagem" do
#       expect(@result.parsed_response["error"]).to eql "required email"
#     end
#   end

#   context "sem o campo email" do
#     before(:all) do
#       payload = { name: "José Manoel", password: "1234" }
#       MongoDB.new.remove_user(payload[:email])
#       @result = Signup.new.create(payload)
#     end

#     it "deve retornar 412" do
#       expect(@result.code).to eql 412
#     end

#     it "deve retornar mensagem" do
#       expect(@result.parsed_response["error"]).to eql "required email"
#     end
